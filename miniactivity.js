

function Pokemon (name, lvl, hp) {
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = lvl,
	this.tackle = function(target) {
		/*console.log(target);*/
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name}'s health is now reduced to ${target.health -= this.attack}`)
		if(target.health < 70){
			target.faint(target.name);
		}

	},
	this.faint = function(target) {
		console.log(`${target} fainted`)
	}
}

let pikachu = new Pokemon("Pikachu", 5, 50);
let charizard = new Pokemon("Charizard", 8, 40);
let squirtle = new Pokemon("Squirtle", 3, 45);
let mew = new Pokemon("Mew", 9, 80);

console.log(squirtle.tackle(charizard));

