console.log("Hello javascript Objects");

const grades = [
	91,
	92,
	93,
	94,
	95
];


const names = ["Joy", "Natalia", "Bianca"];



//Objects are collection of multiple values with different data types
const objGrades = { 

	firstName: "Aaron",

	lastName: "Delos Santos",

	firstGrading: 91,

	subject: "English",

	teachers: ["Carlo", "Masahiro"],

	isActive: true,
	
	schools: {
		city: "Manila",
		country: "Philippines",
	},

	//array of objects
	studentNames: [
	{
		name: "Adrian",
		batch: 152
	},
	{
		name: "Nikko",
		batch: 152
	}

	],
	//function
	description: function() {
		return `${this.subject}: ${this.firstGrading} of students ${this.studentNames[0].name} and ${this.studentNames[1].name}`
	}
}

/* How do we access properties of an object? 
	dot notation(.)
	bracket notation ([""])

	syntax:

	objReference.propertyName
	objectReference["propertyName"]

*/


console.log(objGrades["firstGrading"]);
console.log(objGrades["subject"]);
console.log(objGrades["firstName"], objGrades["lastName"]);
console.log(objGrades.description());
console.log(objGrades.schools.country);
console.log(objGrades["schools"]["city"]);
console.log(objGrades["studentNames"][1]);
console.log(objGrades.studentNames[1]["name"]);

//With the use of .notation & assignment operator 
objGrades.sem = "first";
console.log(objGrades);
delete objGrades.sem;
console.log(objGrades);


const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];


//console.log(["studentGrades"][studentId][0].(studentGrades.Q1 + studentGrades.Q2 + studentGrades.Q3 + studentGrades.Q4) / 4); 
function computeAverage() {
let average = 0;
let studentNumbers = studentGrades.length;
for (let i = 0; i <= studentNumbers; i++) {
	let sumOfQuarters = 0;
	sumOfQuarters = studentGrades[i].Q1 + studentGrades[i].Q2 + studentGrades[i].Q3 + studentGrades[i].Q4;

	average = sumOfQuarters / 4;
	console.log(average);
	console.log("Student " + studentGrades[i].studentId + " has an average of " + average);
	studentGrades[i].average = average;
}

}
console.log(studentGrades);



function _forEachSolution () {
	studentGrades.forEach(function(element) {
		/*console.log(element);*/
		let ave = (element.Q1 + element.Q2 + element.Q3 + element.Q4) / 4;
		element.average = parseFloat(ave.toFixed(1));
	})
}


let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	attack: function() {
		console.log(`This Pokemon tackled targetPokemon`);
		console.log(`TargetPokemon's health is now reduced to targetPokemonhealth`);
	},
	faint: function() {
		console.log(`Pokemon fainted`);
	}
}


console.log(myPokemon);

//Object Constructor
/*function Pokemon (name, lvl, hp) {
	this.name = name,
	this.level = lvl,
	this.health = hp * 2,
	this.attack = lvl,
	this.tackle = function(target) {
		console.log(target);
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
	},
	this.faint = function() {
		console.log(`Pokemon fainted`);
	}
}*/


