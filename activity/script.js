let friends = [
{
	name: "Brock",
	
},
{
	name: "Misty",
	
}
];

let ashpokemons = [
	"Pikachu",
	"Charmander",
	"Onyx",
	"Bayleaf"
];

//trainer OBJECT CONSTRUCTOR
function trainer(name, age, arrayofpokemon, friends) {
	this.name = name,
	this.age = age,
	this.arrayofpokemon = arrayofpokemon,
	this.friend = friends,
	this.talk = function (pokemonname) {
		console.log(`${pokemonname}! I choose you!`);
	}
}

let ash = new trainer("Ash Ketchum", 21, ashpokemons, friends);

console.log(ash);
console.log(ash.talk(ashpokemons[0]));

//createPokemon OBJECT CONSTRUCTOR
function createPokemon(name, lvl) {
	this.name = name,
	this.level = lvl,
	this.health = lvl * 5,
	this.attack = lvl,
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name}'s health is now reduced to ${target.health -= this.attack}`)
		if(target.health <= 0){
			target.faint(target.name);
		}

	},
	this.faint = function(target) {
		console.log(`${target} fainted`)
	}
}

//OBJECT INSTANTIATION
let mew = new createPokemon("Mew", 10);
let blastoise = new createPokemon("Blastoise", 7);
let rayquaza = new createPokemon("Rayquaza", 9);

console.log(blastoise.tackle(rayquaza));



console.log(ashpokemons);

//ADDING POKEMON =)
function capture(newpokemon){
	ashpokemons.unshift(newpokemon);
}

//RELEASING POKEMON =(
function release(){
	ashpokemons.pop();
}


